#!/bin/bash

TYPE=$1
NODE=$2
IP1=$3
IP2=$4

[[ -z $TYPE ]] && echo "TYPE is not specified" && exit 1
[[ -z $NODE ]] && echo "NODE is not specified" && exit 1
[[ -z $IP1 ]] && echo "IP1 is not specified" && exit 1
[[ -z $IP2 ]] && echo "IP2 is not specified" && exit 1

# Формируем hosts
if [[ ! -f hosts ]]; then
echo "# builder" > hosts
echo "192.168.81.100 builder " >> hosts
echo "192.168.82.100 r2_builder" >> hosts
fi
echo "# $NODE" >> hosts
echo "${IP1} ${NODE} r1_${NODE}" >> hosts
echo "${IP2} r2_${NODE}" >> hosts

# Dockerfile узла
DOCKERFILE=Dockerfile
DIRNODE=$NODE-node

[[ -e $DIRNODE ]] && rm -f -r $DIRNODE 
mkdir $DIRNODE
cp ./${TYPE}/template.Dockerfile ./$DIRNODE/$DOCKERFILE
cp ./${TYPE}/init.sh.in ./$DIRNODE/init.sh
cp ./${TYPE}/update.sh.in ./$DIRNODE/update.sh
subst "s|NODE|${NODE}|g;" ./$DIRNODE/$DOCKERFILE
subst "s|NODENAME|${NODE}|g;" ./$DIRNODE/update.sh
subst "s|IPADDRESS1|ip a add \"${IP1}/24\" dev eth0|g;" ./$DIRNODE/init.sh
subst "s|IPADDRESS2|ip a add \"${IP2}/24\" dev eth1|g;" ./$DIRNODE/init.sh

# Docker-compose конфигурация
[[ -f docker-compose.yml ]] || cp template.docker-compose.yml docker-compose.yml
COMPOSE_FILE=docker-compose.yml
COMPOSE_TMP=docker-compose.tmp
REPLACE_TAG="UNITS"
NUM=$(cat ${COMPOSE_FILE} | grep -n ${REPLACE_TAG} | grep -o "[0-9]*")
END_NUM=$(($(cat ${COMPOSE_FILE} | wc -l) - ${NUM}))

#Содержимое до вставки
cat ${COMPOSE_FILE} | grep -m "${NUM}" "" > $COMPOSE_TMP
#Вставка
cat ./${TYPE}/docker-compose.part | sed "s|NODE|${NODE}|g;" >> $COMPOSE_TMP
echo >> $COMPOSE_TMP
#Содержимое после вставки
cat ${COMPOSE_FILE} | tail -${END_NUM} >> $COMPOSE_TMP

cat $COMPOSE_TMP > ${COMPOSE_FILE}
rm -f $COMPOSE_TMP