FROM admiral789/alt-kp8:1.0
#Устанавливаем общие пакеты с официального alt-репозитория(для того, чтобы операции кэшировались, каждую утилиту отдельной командой)
RUN apt-get update
RUN apt-get install -y mc
RUN apt-get install -y ccache
RUN apt-get install -y openssh openssh-server openssh-common openssh-clients openssh-keysign openssh-server-control
RUN apt-get install -y passwd
RUN apt-get install -y su --fix-missing
RUN apt-get install -y netlist --fix-missing
RUN apt-get install -y iptables --fix-missing
RUN apt-get install -y libpoco --fix-missing
RUN apt-get install -y libxml2 --fix-missing
RUN apt-get install -y libomniORB --fix-missing
RUN apt-get install -y libsigc++2 --fix-missing
RUN apt-get install -y xsltproc --fix-missing
RUN apt-get install -y rdate --fix-missing
RUN apt-get install -y libomniORB-names --fix-missing
RUN apt-get install -y xinitrc --fix-missing
RUN apt-get install -y x11vnc --fix-missing
#RUN apt-get install -y tigervnc-server --fix-missing
RUN apt-get install -y libwebkitgtk2 --fix-missing
RUN apt-get install -y libglademm --fix-missing
RUN apt-get install -y libglade --fix-missing
RUN apt-get install -y librsvg --fix-missing
RUN apt-get install -y xinit --fix-missing
RUN apt-get install -y autologin --fix-missing
RUN apt-get install -y net-snmp-clients --fix-missing
RUN apt-get install -y fonts-ttf-google-droid-sans --fix-missing
RUN apt-get install -y postgresql9.4-server --fix-missing
RUN apt-get install -y libpqxx  --fix-missing
RUN sed -i -r "s|(^rpm)|#\1|g;" /etc/apt/sources.list.d/alt.list

#Устанавливаем пакеты с нашего билдера
RUN echo "rpm ftp://builder/pub/Ourside i586 PROJECTNAME" > /etc/apt/sources.list.d/builder.list
RUN echo "rpm ftp://builder/pub/Ourside noarch PROJECTNAME" >> /etc/apt/sources.list.d/builder.list
#RUN apt-get update
#RUN apt-get install -y startup-micro

#Подготовливаем систему
#SSHKEYS
RUN useradd -u 1000 guest
RUN echo "PermitRootLogin yes" >> /etc/openssh/sshd_config
RUN echo "PubkeyAuthentication yes" >> /etc/openssh/sshd_config
RUN echo "PubkeyAcceptedKeyTypes ssh-dss" >> /etc/openssh/sshd_config
WORKDIR /root
COPY ./hosts /root
COPY ./init.sh /root
COPY ./update.sh /root
RUN chmod 'a+x' ./init.sh
RUN chmod 'a+x' ./update.sh

ENTRYPOINT ./init.sh; /bin/sh