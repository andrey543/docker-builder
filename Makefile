# Настройки общие
HOSTDIR=$(shell pwd)
HOSTUSER=${USER}
# uid для установки пользователю прав записи из контейнера (для root uid=0)
CONTAINERUID=1000

# Название билдера
PROJECT=alt

# Плагин(если требуется получение ip от DHCP внешней, относительно докера, сети)
PLUGIN=admiral789/net-dhcp:latest
INTERFACE=eth0
MAC=`ip link show ${INTERFACE} | grep -o "ether [0-9a-f:]*" | sed "s/ether//"`
BRIDGENAME=br-docker
DOCKER_DHCP_NET=dhcp-net

help:
	# make up - Запуск системы
	# make down - Останов системы
	# make up-dhcp - Запуск системы с созданием моста(для доступа к dhcp)
	# make down-dhcp - Останов системы с удалением моста
	# make bridge - Создать мост
	# make rm-bridge - Удалить мост
	# make add-nodes - Добавить узлы системы(в docker-compose)
	# make update-hosts - Обновить hosts-файл
	# make sshkeys - Генерация ssh-ключа для доступа билдера на узлы
	# make permissions-host - Права хоста на чтения/записи ресурсных папок
	# make permissions-container - Права контейнера(билдера) на чтения/записи ресурсных папок
	# make clean - Очистка созданных файлов и папок

# Узлы
add-nodes: add-node.sh
	rm -f docker-compose.yml
	rm -f hosts
	echo "# builder" > hosts
	echo "192.168.81.100 builder " >> hosts
	echo "192.168.82.100 r2_builder" >> hosts
	# Добавлем список узлов
#	./add-node.sh kp8 cpu1 192.168.81.1 192.168.82.1
#	./add-node.sh kp8 cpu2 192.168.81.2 192.168.82.2
#	./add-node.sh kp8 geu1 192.168.81.3 192.168.82.3
#	./add-node.sh kp8 imitator 192.168.81.4 192.168.82.4
	# Обновляем hosts
	make update-hosts
	# Добавляем ssh-ключи
	make sshkeys

# Генерация hosts на основе добавленных nodes
NODEDIRS=$(shell ls | grep node | grep -v .sh )
update-hosts:
	rm -f ./builder/hosts
	rm -f ./*-node/hosts
	cp -f hosts ./builder/
	for n in ${NODEDIRS}; do cp hosts -f ./$$n;done;

# Генерация единого ssh-ключа билдера для доступа к узлам
SSHDIR=./builder/.ssh/
sshkeys:
	[[ -f ${SSHDIR}${PROJECT}.key ]] || ssh-keygen -t dsa -f ${SSHDIR}${PROJECT}.key
	for n in ${NODEDIRS};do cp ${SSHDIR}${PROJECT}.key.pub -f ./$$n/root; \
	subst "s|#SSHKEYS|COPY ./root /etc/openssh/authorized_keys2|g;" ./$$n/Dockerfile;\
	done;

# Развертывание docker-системы с динамическим созданием моста
up-dhcp:
	#Проверка наличия dhcp-плагина. Если отсутствует - устанавливаем
	docker plugin inspect ${PLUGIN} > /dev/null 2>&1 || docker plugin install ${PLUGIN}
	make bridge
	make up

# Остановка системы и удаление динамически созданного моста
down-dhcp:
	make down
	make rm-bridge
	
# Установка прав хоста на общедоступные(с контейнером) файлы/папки
permissions-host:
	sudo chown ${HOSTUSER}:${HOSTUSER} -R ./builder/.jenkins
	sudo chown ${HOSTUSER}:${HOSTUSER} -R ./builder/.ssh
	sudo chown ${HOSTUSER}:${HOSTUSER} -R ./builder/ftp

# Установка прав контейнера на общедоступные(с контейнером) файлы/папки
permissions-container:
	sudo chown ${CONTAINERUID}:${CONTAINERUID} -R ./builder/.jenkins
	sudo chown ${CONTAINERUID}:${CONTAINERUID} -R ./builder/.ssh
	sudo chown ${CONTAINERUID}:${CONTAINERUID} -R ./builder/ftp

# Создание моста
bridge:
	# Создаем мост
	sudo ip link add ${BRIDGENAME} type bridge
	sudo ip link set ${BRIDGENAME} up
	sudo ip link set ${INTERFACE} master ${BRIDGENAME}
	sudo ip link set ${BRIDGENAME} address ${MAC}
	sudo dhcpcd ${BRIDGENAME}

# Удаление моста
rm-bridge:
	# Удаляем мост
	sudo ip link set ${BRIDGENAME} down
	sudo ip link delete ${BRIDGENAME}
	# Ждем, потому что dhcpcd нужно время
	sleep 3
	sudo dhcpcd eth0

# Развертывание docker-системы
up: ./builder/template.Dockerfile template.docker-compose.yml
	# Создаем и запускаем builder-конетейнер ${PROJECT}
	make add-nodes
	[[ -f docker-compose.yml ]] || cp template.docker-compose.yml docker-compose.yml
	cp ./builder/template.Dockerfile ./builder/Dockerfile
	cp ./builder/update-common-libs.sh.in ./builder/update-common-libs.sh
	subst "s|PROJECTNAME|${PROJECT}|g;" ./*-node/Dockerfile || continue
	subst "s|PROJECTNAME|${PROJECT}|g;" ./*-node/*.sh || continue
	subst "s|PROJECTNAME|${PROJECT}|g;" ./builder/update-common-libs.sh
	subst "s|PROJECTNAME|${PROJECT}|g;" ./docker-compose.yml
	subst "s|PROJECTNAME|${PROJECT}|g;" ./builder/Dockerfile
	subst "s|IP|${IP}|g;" ./docker-compose.yml
	subst "s|IN_NET1|${IN_NET1}|g;" ./docker-compose.yml
	subst "s|IN_NET2|${IN_NET2}|g;" ./docker-compose.yml
	subst "s|MAIN_NET|${DOCKER_DHCP_NET}|g;" ./docker-compose.yml
	subst "s|BRIDGE|${BRIDGENAME}|g;" ./docker-compose.yml
	subst "s|PLUGIN|${PLUGIN}|g;" ./docker-compose.yml
	subst "s|HOSTDIR|${HOSTDIR}|g;" ./docker-compose.yml
	make permissions-host
	docker-compose build
	make permissions-container
	docker-compose up -d

# Останов docker-системы
down:
	docker-compose down
	make permissions-host

clean:
	rm -f ./builder/Dockerfile
	rm -f ${SSHDIR}${PROJECT}*
	for n in $(shell ls ./builder | grep .sh | grep -v init.sh | grep -v .in); do rm -f ./builder/$$n;done;
	rm -f -r ./*-node
	rm -f ./docker-compose.yml