#!/bin/sh

J_SERVER=192.168.3.16:8080
J_CLI=~/.jenkins/war/WEB-INF/jenkins-cli.jar

java -jar $J_CLI -s http://$J_SERVER/ list-jobs
exit 0

#IFS for jobs with spaces.
SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
for i in $(java -jar $J_CLI -s http://$J_SERVER/ list-jobs); 
do 
  java -jar $J_CLI -s http://$J_SERVER/ get-job ${i} > ${i}.xml;
done
IFS=$SAVEIFS
mkdir deploy
tar cvfj "jenkins-jobs.tar.bz2" ./*.xml

