FROM alt:latest
#Устанавливаем общие пакеты с официального alt-репозитория
RUN apt-get update
RUN apt-get install -y mc
RUN apt-get install -y su --fix-missing
RUN apt-get install -y sudo
RUN apt-get install -y ccache
RUN apt-get install -y etersoft-build-utils
RUN apt-get install -y automake_1.11
RUN apt-get install -y autoconf_2.60
RUN apt-get install -y openssh openssh-server openssh-common openssh-clients openssh-keysign openssh-server-control
RUN apt-get install -y netlist --fix-missing
RUN apt-get install -y iptables --fix-missing
RUN apt-get install -y xsltproc --fix-missing
RUN apt-get install -y rdate --fix-missing
RUN apt-get install -y apt-repo-tools --fix-missing
RUN apt-get install -y nginx --fix-missing
RUN apt-get install -y netlist --fix-missing
RUN apt-get install -y squid --fix-missing
RUN apt-get install -y wget --fix-missing
RUN apt-get install -y pssh --fix-missing
RUN apt-get install -y vsftpd --fix-missing
RUN apt-get install -y git --fix-missing
RUN apt-get install -y doxygen --fix-missing
RUN apt-get install -y graphviz --fix-missing
RUN apt-get install -y gcc-c++ --fix-missing
RUN apt-get install -y catch --fix-missing
# Для jenkins
RUN apt-get install -y java-1.8.0-openjdk --fix-missing
RUN apt-get install -y java-1.8.0-openjdk-headless --fix-missing
RUN apt-get install -y tzdata-java --fix-missing
RUN apt-get install -y java-common --fix-missing
RUN apt-get install -y javapackages-tools --fix-missing
RUN apt-get install -y libjavascriptcoregtk4 --fix-missing
RUN apt-get install -y rpm-build-java --fix-missing
RUN apt-get install -y rpm-macros-java --fix-missing
RUN apt-get install -y libjavascriptcoregtk2 --fix-missing
RUN apt-get install -y libjavascriptcoregtk2-devel --fix-missing
RUN apt-get install -y python3-module-javapackages --fix-missing
RUN apt-get install -y ca-certificates-java --fix-missing
RUN apt-get install -y javaewah --fix-missing
# Uniset
RUN apt-get install -y libomniORB --fix-missing
RUN apt-get install -y libomniORB-names --fix-missing
RUN apt-get install -y libuniset2 --fix-missing
RUN apt-get install -y libuniset2-devel --fix-missing
RUN apt-get install -y libuniset2-extension-common --fix-missing
RUN apt-get install -y libuniset2-extension-common-devel --fix-missing
RUN apt-get install -y libuniset2-utils --fix-missing
RUN apt-get install -y libuniset2-extension-sqlite-devel --fix-missing
RUN apt-get install -y libuniset2-extension-pgsql-devel --fix-missing
RUN apt-get install -y libuniset2-extension-logicproc-devel --fix-missing
RUN apt-get install -y uniset-configurator --fix-missing
# Доп пакеты
RUN apt-get install -y etcnet --fix-missing

# Конфигурируем систему
RUN mkdir /var/lib/jenkins
RUN mkdir -p /var/ftp/pub/Ourside/i586
COPY ./vsftpd /etc/xinetd.d/vsftpd
COPY ./default.conf /etc/nginx/sites-available.d
COPY ./upgrade-common-libs.sh /usr/local/bin/
COPY ./jenkins /etc/init.d
COPY ./jenkins.war /var/lib/jenkins
RUN ln -s -t /etc/nginx/sites-enabled.d /etc/nginx/sites-available.d/default.conf

# Конфигурируем root-пользователя
WORKDIR /root
COPY ./init.sh /root/
COPY ./update-common-libs.sh /root/
COPY ./hosts /root
RUN sed -i "s|sudo||g;" ./init.sh
RUN sed -i "s|sudo||g;" ./update-common-libs.sh
RUN chmod 'a+x' /root/init.sh
RUN chmod 'a+x' /root/update-common-libs.sh

# Конфигурируем под пользователем jenkins
RUN useradd -u 1000 jenkins
RUN gpasswd -a jenkins wheel
RUN mkdir /home/jenkins/bin/
COPY ./jenkins-wrapper.sh /home/jenkins/bin
COPY ./jenkins-cli.jar /home/jenkins/bin
COPY ./jenkins-job-backup.sh /home/jenkins/bin
RUN echo "jenkins ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/jenkins
RUN [ "chmod","400","/etc/sudoers.d/jenkins" ]
RUN chown jenkins:jenkins -R /home/jenkins
RUN chown jenkins:jenkins -R /var/ftp

ENTRYPOINT ./init.sh; /bin/sh