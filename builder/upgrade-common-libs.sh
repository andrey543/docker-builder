#!/bin/sh
#Кому разрешен вызов скрипта прописано в /etc/sudoers.d/install-common-libs
PROG="${0##*/}"
INSTALL_LIST=

print_help()
{
	cat <<EOF
	Обновляет в системе пакеты на последние версии, лежащие на основном builder'е.

	Use:
		$PROG -key [-key] [-key]
	Keys:
		-u	Обновление libuniset2
		-a	Обновление libuniset2-algorithms
		-w	Обновление libuniwidgets2
		-s	Обновление libsetwidgets2
		--all	Обновление всего

	Example:
		$PROG -u -a		-обновляем libuniset2 и libuniset2-algorithms
EOF
	exit 1

}

upgrade_uniset2()
{
	echo "Обновление libuniset2"
	INSTALL_LIST="$INSTALL_LIST libuniset2 libuniset2-set-release"
}

upgrade_alg()
{
	echo "Обновление libuniset2-algorithms"
	INSTALL_LIST="$INSTALL_LIST libuniset2-algorithms"
}

upgrade_uniwidgets()
{
	echo "Обновление libuniwidgets2"
	INSTALL_LIST="$INSTALL_LIST libuniwidgets2"
}

upgrade_setwidgets()
{
	echo "Обновление libsetwidgets2"
	INSTALL_LIST="$INSTALL_LIST libsetwidgets2"
}

[[ -z "$1" ]] && print_help

#parse command line options
for ARG in "$@";
do
	[[ "$ARG" == "-u" ]] && upgrade_uniset2 && continue
	[[ "$ARG" == "-a" ]] && upgrade_alg && continue
	[[ "$ARG" == "-w" ]] && upgrade_uniwidgets && continue
	[[ "$ARG" == "-s" ]] && upgrade_setwidgets && continue
	[[ "$ARG" == "--all" ]] && upgrade_uniset2 && upgrade_alg && upgrade_uniwidgets && upgrade_setwidgets && break
done

apt-get update
apt-get install -y $INSTALL_LIST
