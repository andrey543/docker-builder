#! /bin/sh

FTP=/var/ftp/pub/Ourside
ARCH_DIRS=$(ls ${FTP})

for ARCH in ${ARCH_DIRS}
do
    genbasedir --progress --topdir=${FTP} ${ARCH}
done