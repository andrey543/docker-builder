#!/bin/sh

#Этот скрипт обновляет rpm-пакеты в ftp репозитории. Также устанавливает новые версии в систему
#для работы использует скрипт /usr/local/bin/upgrade-common-libs.sh

PROG="${0##*/}"
PROJECT=alt

#Для группировки команд
UPDATE_LIST=
UPGRADE_LIST=
UPDATE_URL_LIST=

#Для исключения повторного добавления в _LIST
UNISET2_FLAG=
ALGORITHMS_FLAG=
UNIWIDGETS_FLAG=
SETWIDGETS_FLAG=

#подхватавыем переменные окружения
[ -n "$SETWIDGETS_SKIP" ] && SETWIDGETS_FLAG=1

#Откуда копируем
SYNUSER="jenkins"
SYNHOST="builder-p8.nio14"
FTP_HOSTDIR="/var/ftp/pub/Ourside/i586/"
UNISET2_DIR="RPMS.uniset2/"
ALGORITHMS_DIR="RPMS.libuniset2-algorithms/"
UNIWIDGETS_DIR="RPMS.libuniwidgets2-19910C/"
SETWIDGETS_DIR="RPMS.setwidgets2/"
FTPURL="ftp://${SYNHOST}/pub/Ourside/i586/"

#Куда копируем
FTP_DIR="/var/ftp/pub/Ourside/i586/RPMS.${PROJECT}/"

print_help()
{
	cat << EOF
	Обновляет rpm-пакеты в ftp-репозитории проекта. После чего вызывает /usr/local/bin/upgrade-common-libs.sh, который устанавливает эти пакеты в систему.

	Use:
		$PROG -key [-key] [-key]
	Keys:
		-u	Обновление libuniset2
		-a	Обновление libuniset2-algorithms
		-w	Обновление libuniwidgets2
		-s	Обновление libsetwidgets2
		--all	Обновление всего

	Example:
		$PROG -u -a		-обновляем libuniset2 и libuniset2-algorithms

EOF
	exit 1
}

update_uniset2()
{
	#Чтобы несколько раз не добавлялись в список
	if [[ -z "$UNISET2_FLAG" ]]
	then
		UNISET2_FLAG=1
		UPDATE_LIST="$UPDATE_LIST ${SYNUSER}@${SYNHOST}:${FTP_HOSTDIR}${UNISET2_DIR}"
		UPDATE_URL_LIST="$UPDATE_URL_LIST ${FTPURL}${UNISET2_DIR}/*"
		UPGRADE_LIST="$UPGRADE_LIST -u"
	fi
}

update_alg()
{
	if [[ -z "$ALGORITHMS_FLAG" ]]
	then
		ALGORITHMS_FLAG=1
		UPDATE_LIST="$UPDATE_LIST ${SYNUSER}@${SYNHOST}:${FTP_HOSTDIR}${ALGORITHMS_DIR}"
		UPDATE_URL_LIST="$UPDATE_URL_LIST ${FTPURL}${ALGORITHMS_DIR}/*"
		UPGRADE_LIST="$UPGRADE_LIST -a"
	fi
}

update_uniwidgets()
{
	if [[ -z "$UNIWIDGETS_FLAG" ]]
	then
		UNIWIDGETS_FLAG=1
		UPDATE_LIST="$UPDATE_LIST ${SYNUSER}@${SYNHOST}:${FTP_HOSTDIR}${UNIWIDGETS_DIR}"
		UPDATE_URL_LIST="$UPDATE_URL_LIST ${FTPURL}${UNIWIDGETS_DIR}/*"
		UPGRADE_LIST="$UPGRADE_LIST -w"
	fi
}

update_setwidgets()
{
	if [[ -z "$SETWIDGETS_FLAG" ]]
	then
		SETWIDGETS_FLAG=1
		UPDATE_LIST="$UPDATE_LIST ${SYNUSER}@${SYNHOST}:${FTP_HOSTDIR}${SETWIDGETS_DIR}"
		UPDATE_URL_LIST="$UPDATE_URL_LIST ${FTPURL}${SETWIDGETS_DIR}/*"
		UPGRADE_LIST="$UPGRADE_LIST -s"
	fi
}

[[ -z "$*" ]] && print_help

for ARG in "$@";
do
	[[ "$ARG" == "-u" ]] && update_uniset2 && continue
	[[ "$ARG" == "-a" ]] && update_alg && continue
	[[ "$ARG" == "-w" ]] && update_uniwidgets && continue
	[[ "$ARG" == "-s" ]] && update_setwidgets && continue
	[[ "$ARG" == "--all" ]] && update_uniset2 && update_alg && update_uniwidgets && update_setwidgets && break
done

#Если нет ни одного верного аргумента
[[ -z "$UPDATE_LIST" ]] && echo "	Нет ни одного верного аргумента" && echo "HELP:" && print_help

#Копируем
echo "################################################################"
echo ""
echo "                   ОБНОВЛЯЕТСЯ FTP-РЕПОЗИТОРИЙ                  "
echo ""
echo "################################################################"
#rsync --progress --exclude=backup/ -arz -t ${UPDATE_LIST} ${FTP_DIR}
wget -v -N -P ${FTP_DIR} ${UPDATE_URL_LIST}

#Обновляем базы
echo "################################################################"
echo ""
echo "                        ОБНОВЛЯЮТСЯ БАЗЫ                        "
echo ""
echo "################################################################"
/var/ftp/pub/Ourside/genb.sh


#Обновляем в системе
echo "################################################################"
echo ""
echo "             УСТАНАВЛИВАЮТСЯ НОВЫЕ ВЕРСИИ В СИСТЕМУ             "
echo ""
echo "################################################################"
#upgrade-common-libs.sh ${UPGRADE_LIST}

exit 0

