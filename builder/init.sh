#!/bin/sh

# Подменяем ip
#ip a add "192.168.81.100/24" dev eth0
#ip a add "192.168.82.100/24" dev eth1
# Обновляем хосты
sudo cat /root/hosts >> /etc/hosts
sudo update_chrooted conf
# Обновляем библиотеки
#/home/jenkins/update-common-libs.sh --all
# Запускаем сервисы
service xinetd start
service nginx start
service sshd start
service squid start
service jenkins start
